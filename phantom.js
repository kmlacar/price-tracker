const phantom = require('phantom');

const jsdom = require("jsdom");
const NS_PER_SEC = 1e9;


module.exports.search = function (url, selector, _callBack) {


    (async function () {

        let timeout = setTimeout((function () {
            console.error("timeout!! url="+url);
            _callBack(null);
        }), 20 * 1000);

        console.log("phantom started. url=" + url);
        const time = process.hrtime();
        const instance = await phantom.create(['--ignore-ssl-errors=no', '--load-images=false', '--webdriver-loglevel=NONE', '--debug=false']);

        const page = await instance.createPage();

        page.setting('resourceTimeout', 15000);
        page.on("onResourceTimeout", function (requestData, networkRequest) {
            console.log("timeout = " + requestData['url']);
        });

        page.setting('userAgent', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36');

        await page.on("onResourceRequested", function (requestData, networkRequest, pp, dd, ss) {
            // requestData.headers['user-agent']="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36";;
            // if ((/http.+?\.css/).test(requestData['url']) || requestData.headers['Content-Type'] == 'text/css') {
                // console.log('The url of the request is matching. Aborting: ' + requestData['url']);
                // return false;
            // } else {
            // }
        });

        const status = await page.open(url);
        // console.log("status = ", status);
        if (status === 'fail') {
            console.error("phantom finished. url=" + url + ", status=" + status);
            clearTimeout(timeout);
            _callBack();
            return;
        }

        const content = await page.property('content');
        // console.log(content);

        let response = parseData(content, selector);

        const diff = process.hrtime(time);
        console.log(`took ${diff[0] + diff[1] / NS_PER_SEC} sec`);

        await instance.exit();
        console.info("phantom finished. url=" + url + ", status=" + status);
        clearTimeout(timeout);
        _callBack(response);

    }());
}


function parseData(html, selector) {
    const {JSDOM} = jsdom;
    const dom = new JSDOM(html);
    const $ = (require('jquery'))(dom.window);
    // let aPriceVal = '7-0-2-2-6-1-1';

    // let querySelectorAll = dom.window.document.querySelectorAll(aPriceVal);

    // let $priceVal = $(aPriceVal);
    let $priceVal = getElement2(dom.window.document.body, $("html"), selector);
    // console.log("->" + $HTML.length && $HTML[0].innerHTML);


    if ($priceVal) {
        let priceText = $priceVal.textContent.split('').filter(f => f === '.' || f === ',' || !isNaN(f));
        let price = priceText.join("").trim();
        var numb = price.match(/\d/g);
        numb = numb.join("");
        console.log(`price = '${numb}'`);
        return numb;
    } else {
        console.log("not found!");
        return null;
    }

}

function getElement2(body, $html, indexes) {
    let splitted = indexes.split(",");
    let currentElement = body;


    for (let i = 0; i < splitted.length; i++) {

        let tag = splitted[i].split("-");
        let arrays = [...currentElement.childNodes];
        let filtered = arrays.filter(f => f.tagName === tag[1]);
        if (filtered) {
            currentElement = filtered[parseInt(tag[0])];
        }
        if (!currentElement) {
            return;
        }
    }

    return currentElement;

}

function getElement(body, $html, indexes) {
    let splitted = indexes.split("-");
    let currentElement = body;


    for (let i = 0; i < splitted.length; i++) {
        let splittedElement = splitted[i];
        currentElement = currentElement.children[splittedElement];


        if (!currentElement) {
            return;
        }
    }

    return currentElement;

}