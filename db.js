var mongoDbUser = "kml2";
var mongoDbPass = "adm_kml_123_543__aacb"
var db;
var mongodb = require("mongodb");

var phantom = require('./phantom');


module.exports.connect = function (ready) {

    mongodb.MongoClient.connect(process.env.MONGODB_URI || "mongodb://" + mongoDbUser + ":" + mongoDbPass + "@ds117362.mlab.com:17362/kml",
        function (err, client) {
            if (err) {
                console.log(err);
                process.exit(1);
            }
            db = client.db();
            console.log("Database connection ready");
            ready();
        });
}

module.exports.saveData = function (item, _callBack) {

    (async function () {
        db.collection("EASY_PRICES").findOne({'url': item.url}, function (err, result) {
            if (err) throw err;

            if (result) {
                db.collection("EASY_PRICES").update(
                    {"_id": result._id},
                    {$push: {items: item.items[0]}}
                );

            } else {

                item.active = true;
                item.errorCount = 0;
                db.collection("EASY_PRICES").insert(item).catch(reason => {
                    console.log(reason)
                });
            }
            _callBack();
        });
    }());

}

module.exports.job = function (_callBack) {
    (async function () {
        if (!db) {
            return;
        }
        db.collection("EASY_PRICES").find({"active": true}).toArray().then(function (items) {

            items.forEach(item => {

                phantom.search(item.url, item.jquerySelect, function (price) {
                    if (price) {
                        let date = new Date();
                        let priceItem = {price, date};
                        db.collection("EASY_PRICES").update(
                            {"_id": item._id},
                            {$push: {items: priceItem}},
                            {$set: {errorCount: 0}}
                        );

                    } else {

                        item.errorCount = item.errorCount + 1;
                        if (item.errorCount >= 5) {
                            item.active = false;
                        }

                        db.collection("EASY_PRICES").update({"_id": item._id}, {
                            $set: {
                                errorCount: item.errorCount,
                                active: item.active
                            }
                        });
                    }
                });
            });


        }).finally(() => _callBack());


    }());

}
module.exports.all = function (_callBack) {
    (async function () {
        if (!db) {
            return;
        }
        db.collection("EASY_PRICES").find({"active": true}).toArray().then(function (items) {
            _callBack(items);

        });

    }());

}

module.exports.deleteById = function (id,_callBack) {
    (async function () {
        if (!db) {
            return;
        }


        db.collection("EASY_PRICES").deleteOne({_id: new mongodb.ObjectID(id)}, function(err, obj) {
            if (err) throw err;
            _callBack();
        });

    }());

}