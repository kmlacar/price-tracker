const express = require('express')
const path = require('path')
const http = require('http');
var schedule = require('node-schedule');
var request = require('request');
var mongodb = require("mongodb");
const jsdom = require("jsdom");


const PORT = process.env.PORT || 5002;

var mongoDbUser = "kml";
var mongoDbPass = "adm_kml_123_543__aacb"
var db;

var cronCount = 0;

// var cronTime = '*/30 * * * * *';
var cronTime = '0 0 0/1 ? * * *';


var j = schedule.scheduleJob(cronTime, function (fireDate) {

    // cronCount++;
    run(function (msg) {
        console.log("run."+fireDate);
    });
});


function sendPush(content) {

    return;

    request.post({
        headers: {'content-type': 'application/x-www-form-urlencoded'},
        url: ' https://api.pushed.co/1/push',
        body: "app_key=rBps2kQGSLrPiOto3H0k&app_secret=W0HIEffObQVJLmaAIAywCVyscukqJkr6fexs55QEEeJBtiKoDJEU9OPFKThWYwDm" +
        "&target_type=app&content=" + content
    }, function (error, response, body) {
        // console.log(body);
    });
}

function parseData(html) {

    let list = new Array();

    const {JSDOM} = jsdom;
    const dom = new JSDOM(html);
    const $ = (require('jquery'))(dom.window);

    var items = $("#main_table_countries_today tbody tr");

    for (var i = 0; i < items.length; i++) {
        let row = $(items[i]).find("td");
        let country = {};
        country.name = $(row[0]).text();
        country.totalCases = $(row[1]).text();
        country.totalDeaths = $(row[3]).text();
        country.totalRecovered = $(row[5]).text();
        country.activeCases = $(row[6]).text();
        country.criticalCases = $(row[7]).text();

        list.push(country);
    }

    return list;

}

function reverseFormatNumber(val) {
    if (!val || val === "" || val.trim() === "") {
        return 0;
    }

    var group = new Intl.NumberFormat("en").format(1111).replace(/1/g, '');
    var decimal = new Intl.NumberFormat("en").format(1.1).replace(/1/g, '');
    var reversedVal = val.replace(new RegExp('\\' + group, 'g'), '');
    reversedVal = reversedVal.replace(new RegExp('\\' + decimal, 'g'), '.');
    return Number.isNaN(reversedVal) ? 0 : reversedVal;
}

function processData(datas) {

    let data = {};
    data.date = new Date();
    data.data = datas;

    db.collection('DATA', function (err, collection) {
        collection.find().sort({$natural: -1}).limit(1).next().then(
            function (doc) {

                let pushText = "";


                let count = 0;
                for (var i = 0; i < datas.length; i++) {
                    let item = datas[i];

                    let founded = doc.data.filter(f => f.name === item.name);

                    if (founded && founded[0]) {

                        let newCase = parseInt(reverseFormatNumber(item.totalCases));
                        let oldCase = parseInt(reverseFormatNumber(founded[0].totalCases));
                        let newDeaths = parseInt(reverseFormatNumber(item.totalDeaths));
                        let oldDeaths = parseInt(reverseFormatNumber(founded[0].totalDeaths));

                        let text;

                        if (newCase != oldCase) {
                            text = item.name + " new=" + (newCase - oldCase);
                        }
                        if (newDeaths != oldDeaths) {
                            if (!text) {
                                text = item.name
                            }
                            text += " death=" + (newDeaths - oldDeaths);
                        }

                        if (text) {
                            count++;
                            pushText += text + "\n";

                            if (count === 5) {
                                sendPush(pushText);
                                pushText = "";
                                count = 0;
                            }
                        }
                    }
                }
                if (pushText != "") {
                    sendPush(pushText);
                }


                let total = datas.filter(f => f.name === 'Total:')[0];

                let message = "Total Case\t\t= " + total.totalCases + "\nTotal Death\t\t= " + total.totalDeaths + "\nTotal Recovered\t= " + total.totalRecovered
                    + "\nActive Cases\t= " + total.activeCases + "\nCritical Cases\t= " + total.criticalCases;


                console.log(message);

                sendPush(message);

                db.collection("DATA").insertOne(data);

            }
        );
    });





}

function run(_callback) {

    request("http://www.worldometers.info/coronavirus/",null, (err,res,body) => {
        let datas = parseData(body);
        processData(datas);
        if (_callback) {
            _callback({"result": "ok"});
        }
    });


    // curl.get("https://www.worldometers.info/coronavirus/", null, (err, resp, body) => {
    //     if (resp.statusCode == 200) {
    //         let datas = parseData(body);
    //
    //         processData(datas);
    //
    //         if (_callback) {
    //             _callback({"result": "ok"});
    //         }
    //
    //     }
    //     else {
    //         if (_callback) {
    //             _callback({"result": "nok"});
    //         }
    //     }
    // });
}

function getData(req, res) {
    // res.json("off");
    run(function (msg) {
        res.json(msg);
    });

}


function ready() {
    express()
        .use(express.static(path.join(__dirname, 'public')))
        .set('views', path.join(__dirname, 'views'))
        .set('view engine', 'ejs')
        .get('/', (req, res) => getData(req, res))
        .listen(PORT, () => console.log(`Listening on ${ PORT }`));

}


mongodb.MongoClient.connect(process.env.MONGODB_URI || "mongodb://" + mongoDbUser + ":" + mongoDbPass + "@ds159216.mlab.com:59216/cvd",
    function (err, client) {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        db = client.db();
        console.log("Database connection ready");
        ready();
    });
