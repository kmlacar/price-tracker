const express = require('express')
const path = require('path')
var bodyParser = require('body-parser')
var phantom = require('./phantom');
var db = require('./db');

const PORT = process.env.PORT || 5002;


const cron = require("node-cron");

// var valid = cron.validate('* */3 *  * *');
// console.log(valid);
cron.schedule("0 0 */6 * * *", function () {
    let date = new Date();
    let start = date.getMilliseconds();
    console.log("task run." + date);
    db.job(function () {
        console.log("task finised. time spend=" + (new Date().getMilliseconds() - start) + " ms");
    })
});


// cron.schedule("*/10 * *  * * *", function () {
//
//
// });


function test(req, res) {
    db.all(function (items) {

        res.json(items);
    })

}

function deleteItem(req, res) {
    db.deleteById(req.body.id, function (itms) {

        res.json({'result': 'ok'});
    })
}

function insertNew(req, res) {

    // console.log(req.body);

    let item = req.body;


    phantom.search(item.url, item.jquerySelect, function (price) {
        if (price) {
            let date = new Date();
            let priceItem = {price, date};

            let dbItem = {};

            dbItem.items = new Array();
            dbItem.items.push(priceItem);
            dbItem.title = item.title.length > 50 ? item.title.slice(0, 50) + "..." : item.title;
            dbItem.jquerySelect = item.jquerySelect;
            dbItem.url = item.url;

            db.saveData(dbItem, function () {
            });
            res.json({'result': 'ok', 'value': price});

        } else {
            // res.json({'result':'nok'});
            res.status(404).send('Price not found');
        }
    });


}

db.connect(function ready() {
        express()
            .use(bodyParser.json())
            .use(express.static(path.join(__dirname, 'public')))
            .set('views', path.join(__dirname, 'views'))
            .set('view engine', 'ejs')
            .post('/insert', (req, res) => insertNew(req, res))
            .post('/delete', (req, res) => deleteItem(req, res))
            .get('/test', (req, res) => test(req, res))
            .listen(PORT, () => console.log(`Listening on ${ PORT }`));

    }
);

